End point:https://reqres.in/api/users/2

Request Body:

{
    "name": "Ashwini",
    "job": "Tester"
}

Response Body:

{"name":"Ashwini","job":"Tester","updatedAt":"2023-10-25T13:01:07.386Z"}