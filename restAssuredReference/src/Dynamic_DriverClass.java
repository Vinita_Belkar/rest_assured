import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import commonUtilitypackage.Excel_Data_Reader;

public class Dynamic_DriverClass {

	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		// Step1: Read the Test cases to be executed from the excel file
		// To read anything from excel file we have a method ReadExcelFile
		ArrayList<String> TestCaseList = Excel_Data_Reader.Read_Excel_Data("API_Data.xlsx", "TestCasesToExecute",
				"TestCaseToExecute");
		System.out.println("Test cases in Excel file are:" + TestCaseList);

		// to call everysingle test case we need for loop to iterate so first we
		// extracting the size if the array
		int count = TestCaseList.size();

		for (int i = 1; i < count; i++) {
			String TestCasesToExecute = TestCaseList.get(i);
			System.out.println("Test Cases which are going to be executed:" + TestCasesToExecute);

			// Step 2 Call the TestCasesToExecute on run time by using
			// Java.lang.reflectpackage
			Class<?> Testclass = Class.forName("testClassPackage." + TestCasesToExecute);

			// we have called class now will call the Execute method of the class captured
			// in variable testclass by using java.lang.method
			Method ExecuteMethod = Testclass.getDeclaredMethod("executor");

			// Step4 set the accessibility as true
			ExecuteMethod.setAccessible(true);

			// Create the instance of class captured in test class variable Testclass
			Object InstanceofTestClass = Testclass.getDeclaredConstructor().newInstance();

			// Step6 execute the method captured in variable executeMethod of CLass Capture
			// in Testclass method

			ExecuteMethod.invoke(InstanceofTestClass);
			System.out.println("Execution of Test Case name " + TestCasesToExecute + " is completed");
			System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			System.out.println("Completed");
		}
	}

}