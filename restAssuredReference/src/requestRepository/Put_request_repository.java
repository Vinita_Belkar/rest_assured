package requestRepository;

import java.io.IOException;
import java.util.ArrayList;

import commonUtilitypackage.Excel_Data_Reader;

public class Put_request_repository extends Endpoints {

	public static String Put_request_tc1() throws IOException {

		ArrayList<String> excel_data=Excel_Data_Reader.Read_Excel_Data("API_Data.xlsx","Put_API","Put_TC3");
		//System.out.println(Excel_data);
		String req_name=excel_data.get(1);
		String req_job=excel_data.get(2);

		String Requestbody="{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		System.out.println(Requestbody);
		return Requestbody;
	}
	}


