package requestRepository;

public class Endpoints {

	static String hostname = "https://reqres.in/";

	public static String post_endpoint() {

		String post_url = hostname + "api/users";
		System.out.println(post_url);
		return post_url;

	}

	public static String Patch_endpoint() {

		String patch_URL = hostname + "api/users/2";
		System.out.println(patch_URL);
		return patch_URL;
	}

	public static String Get_endpoint() {

		String get_url=hostname+"api/users?page=2";
		System.out.println(get_url);
		return get_url;
	}

	public static String Put_endpoint() {

		String put_url= hostname+ "api/users/2";
		//System.out.println(put_url);
		return put_url;
	}
	
	public static String Delete_Endpoint() {
		 
		String delete_URL= hostname+"api/users/2";
		System.out.println(delete_URL);
		return delete_URL;
	}
	}
