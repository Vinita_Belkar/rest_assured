package requestRepository;

import java.io.IOException;
import java.util.ArrayList;

import commonUtilitypackage.Excel_Data_Reader;

public class Patch_requestRepository extends Endpoints{

	public static String patch_request_tc1() throws IOException {

		ArrayList<String> exceldata=  Excel_Data_Reader.Read_Excel_Data("API_Data.xlsx", "Patch_API", "Patch_TC2");
		System.out.println(exceldata);
		String req_name= exceldata.get(1);
		String req_job= exceldata.get(2);


	String RequestBody="{\r\n"
			+ "    \"name\": \""+req_name+"\",\r\n"
			+ "    \"job\": \""+req_job+"\"\r\n"
			+ "}";

	return RequestBody;
	}

}
