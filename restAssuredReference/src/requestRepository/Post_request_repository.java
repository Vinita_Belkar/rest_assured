package requestRepository;

import java.io.IOException;
import java.util.ArrayList;

import commonUtilitypackage.Excel_Data_Reader;

public class Post_request_repository extends Endpoints{

	public static String Post_request() throws IOException {

		ArrayList<String> Excel_data=Excel_Data_Reader.Read_Excel_Data("API_Data.xlsx", "Post_API", "Post_TC2");
		//System.out.println(Excel_data);
		String req_name=Excel_data.get(1);
		String req_job=Excel_data.get(2);

		String Requestbody="{\r\n"
				+ "    \"name\": \""+ req_name +"\",\r\n"
				+ "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
		return Requestbody;
	}


}
