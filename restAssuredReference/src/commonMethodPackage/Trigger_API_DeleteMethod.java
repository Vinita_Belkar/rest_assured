package commonMethodPackage;
import static io.restassured.RestAssured.given;

import requestRepository.Endpoints;

public class Trigger_API_DeleteMethod  extends Endpoints {
	
	public static int extract_Delete_StatusCode(String delete_URL) {
		
		int statuscode=given().when().delete(delete_URL).then().extract().response().statusCode();
		System.out.println(statuscode);
		return statuscode;
	}
	}


