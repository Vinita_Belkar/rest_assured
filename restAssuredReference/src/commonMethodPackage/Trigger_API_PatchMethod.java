package commonMethodPackage;

import static io.restassured.RestAssured.given;

import requestRepository.Patch_requestRepository;

public class Trigger_API_PatchMethod extends Patch_requestRepository {

	public static int extract_patch_Statuscode(String RequestBody, String patch_URL) {

		int statuscode = given().header("Content-Type", "application/json").body(RequestBody).when().patch(patch_URL).then()
				.extract().response().statusCode();
		System.out.println(statuscode);
		return statuscode;
	}

	public static String extract_patch_responsebody(String RequestBody, String patch_URL) {

		String Responsebody = given().header("Content-Type", "application/json").body(RequestBody).when().patch(patch_URL)
				.then().extract().response().asString();
		System.out.println(Responsebody);
		return Responsebody;
	}

}
