package commonMethodPackage;

import static io.restassured.RestAssured.given;

import requestRepository.Endpoints;

public class Trigger_Get_Api extends Endpoints{

	public static int extract_get_statuscode(String get_url) {


		int statuscode=given().when().get(get_url).then().extract().response().statusCode();
		System.out.println(statuscode);
		return statuscode;

	}

	public static String extract_get_response(String get_url) {


			String Responsebody=given().when().get(get_url).then().extract().response().asString();
			System.out.println(Responsebody);
			return Responsebody;
	}




}
