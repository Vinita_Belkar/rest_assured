package commonMethodPackage;

import static io.restassured.RestAssured.given;

import requestRepository.Put_request_repository;

public class Trigger_put_api_Method extends Put_request_repository {

	public static int extract_put_statuscode(String Requestbody, String put_url) {

		int statuscode = given().header("Content-Type", "application/json").body(Requestbody).when().put(Put_endpoint()).then()
				.extract().response().statusCode();
		System.out.println(statuscode);
		return statuscode;
	}

	public static String extract_put_response(String Requestbody, String put_url) {

		String Responsebody = given().header("Content-Type", "application/json").body(Requestbody).when().put(Put_endpoint())
				.then().extract().response().asString();
		System.out.println(Responsebody);
		return Responsebody;
	}

}
