package commonMethodPackage;
import static io.restassured.RestAssured.given;

import requestRepository.Post_request_repository;

public class Trigger_API_PostMethod extends Post_request_repository {


	public static int Extract_API_post_statuscode(String requestbody , String post_url ) {

		int statuscode = given().header("Content-Type", "application/json").body(requestbody).when().post(post_url ).then()
				.extract().response().statusCode();
		System.out.println(statuscode);
		return statuscode;
	}

	public static String Extract_API_POST_RESPONSE(String requestbody , String post_url) {

		String Responsebody= given().header("Content-Type", "application/json").body(requestbody).when().post(post_url).then()
				.extract().response().asString();
		System.out.println(Responsebody);
		return Responsebody;

	}


}
