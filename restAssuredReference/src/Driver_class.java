import java.io.IOException;

import testClassPackage.Delete_tc1;
import testClassPackage.Get_tc1;
import testClassPackage.Patch_tc1;
import testClassPackage.Post_tc1;
import testClassPackage.Put_tc1;

public class Driver_class {

	public static void main(String[] args) throws IOException {

		Post_tc1.executor();

		Patch_tc1.executor();

		//Handle_Api_logs.create_log_directory("file");
		//Excel_Data_Reader.Read_Excel_Data("API_Data.xlsx", "Post_API", "Post_TC1");
		//Excel_Data_Reader.Read_Excel_Data("API_Data.xlsx", "Put_API", "Put_TC1");

		Put_tc1.executor();
		
		Delete_tc1.executor();
		 Get_tc1.executor();
	}


}
