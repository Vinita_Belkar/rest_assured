package testClassPackage;

import java.io.File;
import java.io.IOException;

import commonMethodPackage.Trigger_API_DeleteMethod;
import commonUtilitypackage.Handle_API_LOGS;

public class Delete_tc1 extends Trigger_API_DeleteMethod{
	
	public static void executor() throws IOException {
		
		File dirname=Handle_API_LOGS.create_log_directory("Delete_tc1");
		
		int Statuscode= Trigger_API_DeleteMethod.extract_Delete_StatusCode(Delete_Endpoint());
		System.out.println(Statuscode);
		Handle_API_LOGS.evidence_creator(dirname,"Delete_tc1", Delete_Endpoint(), null, null);
	
	}
	

}
