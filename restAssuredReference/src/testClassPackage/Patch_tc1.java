package testClassPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonMethodPackage.Trigger_API_PatchMethod;
import commonUtilitypackage.Handle_API_LOGS;
import io.restassured.path.json.JsonPath;

public class Patch_tc1 extends Trigger_API_PatchMethod {
	@Test
	public static void executor() throws IOException {

		String Requestbody=patch_request_tc1();
		
		File dirname= Handle_API_LOGS.create_log_directory("Patch_tc1");
		// retrying request
		for (int i = 0; i < 5; i++) {
			int statuscode = Trigger_API_PatchMethod.extract_patch_Statuscode( Requestbody, Patch_endpoint());
			//System.out.println(statuscode);

			if (statuscode == 200) {
				String Responsebody = Trigger_API_PatchMethod.extract_patch_responsebody( Requestbody, Patch_endpoint());
				validator( Requestbody,Responsebody);
				Handle_API_LOGS.evidence_creator(dirname,"Patch_tc1",Patch_endpoint(),patch_request_tc1(),Responsebody);
				//System.out.println(Responsebody);
				break;
			} else {
				System.out.println("Invalid response found hence retrying");
			}
		}
	}

	// parse the response body using jsonpath class
	public static void validator(String Requestbody,String Responsebody) throws IOException {
		JsonPath jsp = new JsonPath(Responsebody);
		String res_name = jsp.getString("name");

		String res_job = jsp.getString("job");
		System.out.println(res_job);

		String res_updatedAt = jsp.getString("updatedAt");
		//System.out.println(res_updatedAt);

		String res_updateAt1 = res_updatedAt.substring(0, 10);
		System.out.println(res_updateAt1);

		LocalDateTime current_date = LocalDateTime.now();
		String systemDate = current_date.toString().substring(0, 10);
		//System.out.println(systemDate);

		JsonPath jsp_req = new JsonPath(Requestbody);
		String req_name = jsp_req.getString("name");
		System.out.println(req_name);

		String req_job = jsp_req.getString("job");
		System.out.println(req_job);

		// validate the response body parameter
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		//Assert.assertEquals(res_updateAt1, systemDate);


	}
}
