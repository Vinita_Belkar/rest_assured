package testClassPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonMethodPackage.Trigger_put_api_Method;
import commonUtilitypackage.Handle_API_LOGS;
import io.restassured.path.json.JsonPath;

public class Put_tc1 extends Trigger_put_api_Method {
	@Test
	public static void executor() throws IOException {
		String requestbody = Put_request_tc1();

		File dirname = Handle_API_LOGS.create_log_directory("Put_tc1");
		// retrying request
		for (int i = 0; i < 5; i++) {
			int statuscode = Trigger_put_api_Method.extract_put_statuscode(requestbody, Put_endpoint());
			// System.out.println(statuscode);

			if (statuscode == 200) {

				String Responsebody = Trigger_put_api_Method.extract_put_response(requestbody, Put_endpoint());
				// System.out.println(Responsebody);
				 Handle_API_LOGS.evidence_creator(dirname,"Put_tc1", Put_endpoint(),Put_request_tc1(), Responsebody);
				// Put_request_tc1(), Responsebody);
				validator(requestbody,Responsebody);
				break;

			} else {
				System.out.println("Invalid response found hence retrying");
			}
		}
	}
	
	public static void validator(String requestbody, String Responsebody) throws IOException {
		// parse the response body using jsonpath class
		JsonPath jsp = new JsonPath(Responsebody);
		String res_name = jsp.getString("name");

		String res_job = jsp.getString("job");
		// System.out.println(res_job);

		String res_updatedAt = jsp.getString("updatedAt");
		// System.out.println(res_updatedAt);

		String res_updatedAt1 = res_updatedAt.substring(0, 10);
		// System.out.println(res_updateAt1);

		LocalDateTime current_date = LocalDateTime.now();
		String systemDate = current_date.toString().substring(0, 10);
		// System.out.println(systemDate);

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		// System.out.println(req_name);

		String req_job = jsp_req.getString("job");
		// System.out.println(req_job);

		// validate the response body parameter
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		//Assert.assertEquals(res_updatedAt1, systemDate);

	}
}
