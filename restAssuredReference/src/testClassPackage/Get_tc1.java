package testClassPackage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonMethodPackage.Trigger_Get_Api;
import commonUtilitypackage.Handle_API_LOGS;
import io.restassured.path.json.JsonPath;

public class Get_tc1 extends Trigger_Get_Api {
	@Test
	public static void executor() throws IOException {

		
		File dirname1 = Handle_API_LOGS.create_log_directory("Get_tc1");
		for (int i = 0; i < 5; i++) {
			int statuscode = Trigger_Get_Api.extract_get_statuscode(Get_endpoint());
			System.out.println("statuscode:" + statuscode);

			if (statuscode == 200) {

				String Responsebody = Trigger_Get_Api.extract_get_response(Get_endpoint());
				System.out.println(Responsebody);
				Handle_API_LOGS.evidence_creator(dirname1, "Get_tc1",  Get_endpoint(),"Get_request()", Responsebody);
				validator(Responsebody);
				break;
			} else {
				System.out.println("Invalid status code hance retrying");
			}
		}
	}

	public static void validator(String Responsebody) {
		// Parse the response body parameter by using JsonPath's class
		

		String[] exp_id_array = { "7", "8", "9", "10", "11", "12" };
		String[] exp_email_array = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] exp_first_name_array = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] exp_last_name_array = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] exp_avatar_array = { "https://reqres.in/img/faces/7-image.jpg",
				"https://reqres.in/img/faces/8-image.jpg", "https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg", "https://reqres.in/img/faces/11-image.jpg",
				"https://reqres.in/img/faces/12-image.jpg" };
		
		JsonPath jsp_get = new JsonPath(Responsebody);
		// fetching the page into variable
		String res_page = jsp_get.getString("page");

		// response contains data of array so fetching the data which will return list
		List<Object> res_data = jsp_get.getList("data");
		int count = res_data.size();
		//after knwoing the count/size of data will do the for loop 
		for (int i = 0; i < count; i++) {

			//the responsebody we extracted above will store it in another variable by giving arguments as i
			String exp_id = exp_id_array[i];
			String exp_email = exp_email_array[i];
			String exp_firstname = exp_first_name_array[i];
			String exp_lastname = exp_last_name_array[i];
			String exp_avatar = exp_avatar_array[i];
			
			// now we will fetch the corresponding repsonsebody parametera from data

			String res_id = jsp_get.getString("data ["+ i + "].id");
			String res_email = jsp_get.getString("data[" + i + "].email");
			String res_first_name = jsp_get.getString("data[" + i + "].first_name");
			String res_last_name = jsp_get.getString("data[" + i + "].last_name");
			String res_avatar = jsp_get.getString("data[" + i + "].avatar");
			System.out.println(res_id);

			Assert.assertNotNull(res_id);
			Assert.assertEquals(res_email, exp_email);
			Assert.assertEquals(res_first_name, exp_firstname);
			Assert.assertEquals(res_last_name, exp_lastname);
			Assert.assertEquals(res_avatar, exp_avatar);

		}

	}
}