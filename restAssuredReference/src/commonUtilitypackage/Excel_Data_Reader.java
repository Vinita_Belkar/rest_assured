package commonUtilitypackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Data_Reader {

	public static ArrayList<String> Read_Excel_Data(String Filename, String Sheetname, String Test_case_name)
			throws IOException {

		ArrayList<String> Arraydata = new ArrayList<>();

		// step1 Locate the file
		String Project_DIR = System.getProperty("user.dir");
		System.out.println(Project_DIR);
		FileInputStream fis = new FileInputStream(Project_DIR + "//Inputdata//" + Filename);

		// Access the located Excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// Count the number of sheets available is excel file
		int Countofsheet = wb.getNumberOfSheets();
		System.out.println(Countofsheet);

		// Access the desired sheet
		for (int i = 0; i < Countofsheet; i++) {
			String sheetname = wb.getSheetName(i);
			if (sheetname.equals(Sheetname)) {
				System.out.println("inside the sheet :" + sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				for (Row currentRow : sheet) {

					// Step 5:- Access the row corresponding desired test Case

					if (currentRow.getCell(0).getStringCellValue().equals(Test_case_name)) {
						Iterator<Cell> Cell = currentRow.iterator();
						while (Cell.hasNext()) {
							String Data = Cell.next().getStringCellValue();
							// System.out.println(Data);
							Arraydata.add(Data);
						}
					}

				}
			}

		}
		wb.close();
		return Arraydata;

	}

}